import { keyListener } from "./startTyping.mjs";

export default function stopGame(socket) {
  document.removeEventListener("keydown", keyListener);
  setDefaultProperties(socket);
}
function setDefaultProperties(socket) {
  hideGameProcess();
  showRoomButtons();
  clearProgress();
  setUnreadyColor();
}
function hideGameProcess() {
  const gameTimer = document.getElementById("game-timer");
  const textBlock = document.getElementById("text-block");
  gameTimer.remove();
  textBlock.remove();
}
function showRoomButtons() {
  const readyButton = document.getElementById("ready-button");
  const backButton = document.getElementById("back-button");
  readyButton.style.display = "block";
  backButton.style.display = "block";
  readyButton.innerText = "READY";
}
function clearProgress() {
  const progressBarsHTML = document.getElementsByClassName("progress");
  const progressBars = [...progressBarsHTML];
  progressBars.forEach((bar) => {
    bar.style.width = "0%";
  });
}

function setUnreadyColor() {
  const statuses = [...document.getElementsByClassName("rdy-status")];
  statuses.forEach((status) => {
    status.className = status.className.replace(" ready", " unready");
  });
}
