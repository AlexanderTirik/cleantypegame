import { IServer } from "../types/serverType";
import { ICommentator } from "../types/commentatorType";
import {
  commentatorModel,
  userModel,
  transportModel,
  roomModel,
} from "../models";
import { IUser } from "../types/userType";
import { Socket } from "socket.io";
import { timer } from "../types/timerType";
import { SECONDS_SHOW_FACT, SECONDS_SHOW_RACE_STATE } from "../socket/config";

function greeting(server: IServer, roomId: string) {
  const state: ICommentator | undefined = commentatorModel.getState("greeting");
  if (state) {
    commentatorModel.setFace(server, state.face, roomId);
    commentatorModel.setPhrase(server, state.phrase[0], roomId);
  }
}

function presentation(server: IServer) {
  const { socket } = server;
  const state: ICommentator | undefined = commentatorModel.getState(
    "presentation"
  );
  if (state) {
    commentatorModel.setFace(server, state.face);
    commentatorModel.setPhrase(server, state.phrase[0]);
    const users = userModel.getAllGameLobbyUsers({ socket });
    const usersPresentation = users.map((user, i) => {
      return `${i + 1}. ${
        user.username
      } on ${transportModel.getRandomTransport()}`;
    });
    commentatorModel.addPhrase(server, usersPresentation.join("\n"));
  }
}

function startTimerBeforeGame(server: IServer) {
  const state: ICommentator | undefined = commentatorModel.getState(
    "startTimer"
  );
  if (state) {
    commentatorModel.setFace(server, state.face);
    commentatorModel.setPhrase(server, state.phrase[0]);
  }
}

function stateOfRace(server: IServer) {
  const { socket } = server;
  const numberOfState = Math.round(Math.random() + 1); // 1 or 2
  const state: ICommentator | undefined = commentatorModel.getState(
    `state${numberOfState}`
  );
  if (state) {
    commentatorModel.setFace(server, state.face);
    const results = roomModel.getResults({ socket });
    const textResult = getTextResults(results!, state.phrase);
    const allResults = "\n All results: \n".concat(getAllResults(results!));
    const fullResults = textResult.concat(allResults);
    commentatorModel.setPhrase(server, fullResults);
  }
}

function getAllResults(results: IUser[]) {
  let fullResult = [];
  fullResult = results.map((user, i) => {
    return `${i + 1}. Name: ${user.username}. Progress: ${user.progress}%.`;
  });
  return fullResult.join("\n");
}

function finishNear(server: IServer) {
  if (finishNearNotCalled(server)) {
    const { socket } = server;
    const state: ICommentator | undefined = commentatorModel.getState(
      "finishNear"
    );
    if (state) {
      commentatorModel.setFace(server, state.face);
      const results = roomModel.getResults({ socket });
      const textResult = getTextResults(results!, state.phrase);
      commentatorModel.setPhrase(server, textResult);
    }
  }
}

function finishNearNotCalled(server: IServer) {
  const { socket } = server;
  const results = roomModel.getResults({ socket });
  const user = userModel.getOne({ socket });
  const userIndex = results!.indexOf(user!);
  if (userIndex != 0) return false;
  else return true;
}

function getTextResults(results: IUser[], phrases: string[]) {
  let fullResult = [];
  let resultsLength = results.length < 4 ? results.length : 3;

  fullResult.push(phrases[0]);
  for (let i = 1; i <= resultsLength; i++) {
    if (results[i - 1]) fullResult.push(results[i - 1].username);
    fullResult.push(phrases[i]);
  }
  return fullResult.join("");
}

function onFinish(server: IServer, user: IUser) {
  const { socket } = server;
  const state: ICommentator | undefined = commentatorModel.getState("onFinish");
  if (state) {
    commentatorModel.setFace(server, state.face);
    commentatorModel.setPhrase(
      server,
      `${state.phrase[0]}${user.username}${state.phrase[1]}`
    );
  }
}

function announceResults(server: IServer) {
  const { socket } = server;
  const state: ICommentator | undefined = commentatorModel.getState("results");
  if (state) {
    commentatorModel.setFace(server, state.face);
    const finishResults = getFinishResults(socket, state.phrase);
    commentatorModel.setPhrase(server, finishResults);
  }
}

function getFinishResults(socket: Socket, phrases: string[]) {
  let textResult = [];
  const results = roomModel.getResults({ socket });
  const gameStartTime = roomModel.getOne({ socket })!.startedTime.getTime();
  if (results!.length >= 3) {
    textResult.push(phrases[0]);
    textResult.push(results![0].username);
    textResult.push(phrases[1]);
    textResult.push(results![1].username);
    textResult.push(phrases[2]);
    const gapBetween1and2 = msToTime(
      parseDate(results![1].lastClick).getTime() -
        parseDate(results![0].lastClick).getTime()
    );
    textResult.push(gapBetween1and2);
    textResult.push(phrases[3]);
    textResult.push(results![2].username);
    textResult.push(phrases[4]);
    const gapBetween1and3 = msToTime(
      parseDate(results![2].lastClick).getTime() -
        parseDate(results![0].lastClick).getTime()
    );
    textResult.push(gapBetween1and3);
    textResult.push(phrases[5]);
  }

  textResult.push("\n All results: \n");
  results!.forEach((user, i) => {
    const gameTime = msToTime(
      parseDate(user.lastClick).getTime() - gameStartTime
    );

    textResult.push(
      `${i + 1}. Name: ${user.username}. Progress: ${
        user.progress
      }. Game time: ${gameTime} \n`
    );
  });
  return textResult.join("");
}

function parseDate(date: string | Date) {
  return new Date(date);
}

function msToTime(milliseconds: number) {
  let minute, seconds, millisec;
  millisec = milliseconds % 1000;
  seconds = Math.floor(milliseconds / 1000);
  minute = Math.floor(seconds / 60);
  seconds = seconds % 60;
  return `${minute}:${seconds}:${millisec}`;
}

function showRandomFact(server: IServer) {
  const state: ICommentator | undefined = commentatorModel.getState("facts");
  if (state) {
    commentatorModel.setFace(server, state.face);
    commentatorModel.setPhrase(server, commentatorModel.getRandomFact());
  }
}

function startIntervalFactes(server: IServer) {
  startTimer(server, SECONDS_SHOW_FACT, () => showRandomFact(server));
}
function startIntervalStates(server: IServer) {
  startTimer(server, SECONDS_SHOW_RACE_STATE, () => stateOfRace(server));
}

function startTimer(server: IServer, interval: number, commentFunc: Function) {
  const { socket } = server;
  const roomId = roomModel.getRoomId({ socket });

  let timerId: timer = setTimeout(function tick() {
    commentFunc();
    timerId = setTimeout(tick, interval * 1000);
    roomModel.addTimer({ roomId }, timerId);
  }, interval * 1000);
  roomModel.addTimer({ roomId }, timerId);
}

export default {
  greeting,
  presentation,
  startTimerBeforeGame,
  finishNear,
  onFinish,
  announceResults,
  startIntervalFactes,
  startIntervalStates,
};
