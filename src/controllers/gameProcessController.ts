import { IServer } from "../types/serverType";
import { roomModel, textModel, userModel } from "../models";
import emitHelper from "../helpers/emitHelper";
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  MAXIMUM_USERS_FOR_ONE_ROOM,
} from "../socket/config";
import { timer } from "../types/timerType";
import commentatorController from "./commentatorController";

function startGame(server: IServer) {
  const { socket } = server;
  const roomId = roomModel.getRoomId({ socket });
  roomModel.setRoomStartedStatus({ socket });

  emitHelper.toServer(server, "DISAPPEAR_ROOM", roomId);

  const textId = getRandomTextId();

  emitHelper.toRoom(server, roomId!, "START_GAME");
  emitHelper.toRoom(server, roomId!, "GET_TEXT_ID", textId);

  startTimerBeforeGame(server);
}

function getRandomTextId() {
  const numberOfTexts = textModel.getNumberOfTexts();
  return randomInteger(0, numberOfTexts - 1);
}

function randomInteger(min: number, max: number): number {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

function startTimerBeforeGame(server: IServer) {
  const { socket } = server;
  commentatorController.presentation(server);
  const secondsTimerBeforGame = SECONDS_TIMER_BEFORE_START_GAME;
  startTimer(
    server,
    secondsTimerBeforGame,
    "TRANSLATE_TIMER_BEFORE_START",
    startGameTimer
  );
}

function startGameTimer(server: IServer) {
  const { socket } = server;
  const gameSeconds = SECONDS_FOR_GAME - 1;
  commentatorController.startIntervalFactes(server);
  commentatorController.startIntervalStates(server);
  roomModel.setRoomStartDate({ socket });
  const roomId = roomModel.getRoomId({ socket });
  emitHelper.toRoom(server, roomId!, "TRANSLATE_GAME_TIMER", gameSeconds + 1);
  startTimer(server, gameSeconds, "TRANSLATE_GAME_TIMER", stopGame);
}

function stopGame(server: IServer) {
  const { socket } = server;
  const roomId = roomModel.getRoomId({ socket });

  const results = roomModel.getResults({ socket });
  emitHelper.toRoom(server, roomId!, "STOP_GAME", results);

  commentatorController.announceResults(server);

  roomModel.setDefaultRoom({ roomId });

  const room = roomModel.getOne({ socket });

  if (room!.users.length < MAXIMUM_USERS_FOR_ONE_ROOM)
    emitHelper.toServer(server, "APPEAR_ROOM", roomId);
}

function startTimer(
  server: IServer,
  seconds: number,
  translateAddress: string,
  finishFunction: Function
) {
  const { socket } = server;
  const roomId = roomModel.getRoomId({ socket });

  let secondsToFinish = seconds;

  let timerId: timer = setTimeout(function tick() {
    emitHelper.toRoom(server, roomId!, translateAddress, secondsToFinish);
    if (secondsToFinish == 0) {
      finishFunction(server);
      return;
    }
    secondsToFinish -= 1;
    timerId = setTimeout(tick, 1000);
    roomModel.addTimer({ roomId }, timerId);
  }, 1000);
  roomModel.addTimer({ roomId }, timerId);
}

function shouldStopGame(server: IServer): boolean {
  const { socket } = server;
  const room = roomModel.getOne({ socket });
  const allUsersFinished: boolean = room!.users.every(
    (user) => user.progress >= 100
  );
  return allUsersFinished;
}

function shouldStartGame(server: IServer): boolean {
  const { socket } = server;
  const room = roomModel.getOne({ socket });
  const allUsersReady: boolean = room!.users.every((user) => user.ready);
  return allUsersReady;
}
export default { startGame, stopGame, shouldStopGame, shouldStartGame };
