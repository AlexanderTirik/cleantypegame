import { IServer } from "../types/serverType";

function toSender(server: IServer, address: string, ...args: any[]) {
  const { socket } = server;
  socket.emit(address, ...args);
}

function toRoom(
  server: IServer,
  roomId: string,
  address: string,
  ...args: any[]
) {
  const { io } = server;
  io.in(roomId).emit(address, ...args);
}

function toServer(server: IServer, address: string, ...args: any[]) {
  const { io } = server;
  io.emit(address, ...args);
}

function toRoomExceptSender(
  server: IServer,
  roomId: string,
  address: string,
  ...args: any[]
) {
  const { socket } = server;
  socket.to(roomId).emit(address, ...args);
}

export default { toSender, toRoom, toServer, toRoomExceptSender };
