import { IServer } from "../types/serverType";
import { userModel, roomModel } from "../models";
import emitHelper from "../helpers/emitHelper";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";
import gameProcessController from "../controllers/gameProcessController";
import commentatorController from "../controllers/commentatorController";

function addUserToRoom(server: IServer, roomId: string) {
  const { socket } = server;
  const username = userModel.getUsername({ socket });

  userModel.addToRoom({ roomId, socket });

  socket.join(roomId);

  const roomOnline = roomModel.getRoomOnline({ roomId });
  emitHelper.toServer(server, "CHANGE_ROOM_ONLINE", roomId, roomOnline);

  if (shouldDisappear(roomOnline))
    emitHelper.toServer(server, "DISAPPEAR_ROOM", roomId);

  const allUsersInRoom = userModel.getAllGameLobbyUsers({ roomId });
  emitHelper.toSender(server, "GET_ALL_USERS_IN_ROOM", roomId, allUsersInRoom);

  emitHelper.toRoomExceptSender(
    server,
    roomId,
    "ADD_USER_IN_ROOM",
    username,
    false
  );
  
  
  commentatorController.greeting(server, roomId)
  
}

function shouldDisappear(roomOnline: number): boolean {
  return roomOnline >= MAXIMUM_USERS_FOR_ONE_ROOM;
}

function deleteUserFromRoom(server: IServer) {
  const { io, socket } = server;
  const username = userModel.getUsername({ socket });
  const roomId = roomModel.getRoomId({ socket });
  userModel.deleteFromRoom({ socket });
  socket.leave(roomId!);

  emitHelper.toRoom(server, roomId!, "DELETE_USER_FROM_ROOM", username);
  const roomOnline = roomModel.getRoomOnline({ roomId });
  if (shouldDelete(roomOnline)) {
    emitHelper.toServer(server, "DELETE_ROOM", roomId);
    roomModel.deleteOne({ roomId });
    return;
  }

  const roomStatus = roomModel.getOne({ roomId })!.status;
  if (shouldAppear(roomOnline, roomStatus))
    emitHelper.toServer(server, "APPEAR_ROOM", roomId);

  emitHelper.toServer(server, "CHANGE_ROOM_ONLINE", roomId, roomOnline);

  if (gameProcessController.shouldStopGame(server)) {
    gameProcessController.stopGame(server);
  }
}

function shouldDelete(roomOnline: number): boolean {
  return roomOnline == 0;
}

function shouldAppear(roomOnline: number, roomStatus: string): boolean {
  return roomOnline < MAXIMUM_USERS_FOR_ONE_ROOM && roomStatus == "created";
}

function changeUserReady(server: IServer) {
  const { socket } = server;
  const roomId = roomModel.getRoomId({ socket });
  const user = userModel.getOne({ socket });

  user!.ready = changeReady(user!.ready);

  const username = userModel.getUsername({ socket });
  emitHelper.toRoom(server, roomId!, "SWAP_READY_STATUS", username);

  if (gameProcessController.shouldStartGame(server)) {
    gameProcessController.startGame(server);
  }
}

function changeReady(status: boolean) {
  return !status;
}

function changeUserProgress(
  server: IServer,
  progress: number,
  lastClick: Date
) {
  const { socket } = server;
  const user = userModel.getOne({ socket });

  user!.progress = progress;
  user!.lastClick = lastClick;

  if (progress == 100) {
    commentatorController.onFinish(server, user!);
  }

  const roomId = roomModel.getRoomId({ socket });
  emitHelper.toRoom(
    server,
    roomId!,
    "CHANGE_PROGRESS_BAR",
    user!.username,
    progress
  );

  if (gameProcessController.shouldStopGame(server)) {
    gameProcessController.stopGame(server);
  }
}
export default {
  addUserToRoom,
  deleteUserFromRoom,
  changeUserReady,
  changeUserProgress,
};
