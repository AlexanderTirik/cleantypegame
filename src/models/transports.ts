import { transports } from "../data"

function getRandomTransport(){
    return transports[Math.floor(Math.random() * transports.length)];
}

export default {getRandomTransport}