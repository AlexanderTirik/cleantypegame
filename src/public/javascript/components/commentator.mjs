function setCommentator(face) {
  const commentator = document.getElementById("commentator");
  commentator.innerText = face;
}

function setPhrase(phrase) {
  const phraseBlock = document.getElementById("phrase");
  phraseBlock.innerText = phrase;
}

function addPhrase(phrase) {
  const phraseBlock = document.getElementById("phrase");
  phraseBlock.innerText = phraseBlock.innerText.concat(phrase);
}

export default { setCommentator, setPhrase, addPhrase };
