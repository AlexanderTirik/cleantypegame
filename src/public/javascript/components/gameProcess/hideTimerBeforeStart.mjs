export default function hideTimerBeforeStart() {
  const timer = document.getElementById("timer-before");
  timer.remove();
}
