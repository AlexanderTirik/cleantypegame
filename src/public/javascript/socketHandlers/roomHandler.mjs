import initGamePage from "../components/gamePage.mjs";
import userInRoomComponent from "../components/userInRoom.mjs";

function getAllUsers(socket, roomId, users) {
  users.forEach((user) => {
    userInRoomComponent.addUserInRoom(user.username);
    userInRoomComponent.setReadyStatus(user.username, user.ready);
  });
  initGamePage(socket, sessionStorage.username, roomId);
}

function addUser(username, ready) {
  userInRoomComponent.addUserInRoom(username);
  userInRoomComponent.setReadyStatus(username, ready);
}

function deleteUser(username) {
  userInRoomComponent.deleteUserFromRoom(username);
}

function swapReadyStatus(username) {
  userInRoomComponent.swapReadyColor(username);
}

export default { getAllUsers, addUser, deleteUser, swapReadyStatus };
