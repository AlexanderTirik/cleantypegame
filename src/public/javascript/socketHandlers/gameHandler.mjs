import gameProcessComponent from "../components/gameProcess/index.mjs";

function startGame() {
  gameProcessComponent.hideButtons();
  gameProcessComponent.showTimerBeforeStart();
}

function translateTimerBeforeStart(socket, second) {
  gameProcessComponent.changeTimerBeforeStart(second);
  if (gameProcessComponent.isTimerFinished(second)) {
    gameProcessComponent.hideTimerBeforeStart();
    gameProcessComponent.showGame(sessionStorage.getItem("gameText"));
    gameProcessComponent.startTyping(socket);
  }
}

async function requestText(textId) {
  const res = await fetch(`http://127.0.0.1:3002/game/texts/${textId}`);
  const response = await res.json();
  const data = response.data;
  sessionStorage.setItem("gameText", data);
}

function translateGameTimer(second) {
  gameProcessComponent.changeGameTimer(second);
}

function changeProgress(username, progress) {
  gameProcessComponent.changeProgressBar(username, progress);
}

function stopGame() {
  gameProcessComponent.stopGame();
}

export default {
  startGame,
  requestText,
  translateTimerBeforeStart,
  translateGameTimer,
  changeProgress,
  stopGame,
};
