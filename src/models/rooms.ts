import { rooms } from "../data";
import { Socket } from "socket.io";
import { IRoom } from "../types/roomType";
import { userModel } from "./index";
import { IUser } from "../types/userType";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";
import { timer } from "../types/timerType";
function addOne(where: { roomId: string }): void {
  const { roomId } = where;
  const emptyRoom: IRoom = {
    users: [],
    timers: [],
    status: "created",
    numberOfFinishedUsers: 0,
    startedTime: new Date(0),
  };
  if (roomId) {
    rooms.set(roomId, emptyRoom);
  }
}

function getAll(where: { status: string; size?: number }) {
  let { size = MAXIMUM_USERS_FOR_ONE_ROOM, status } = where;
  let allRooms: Map<string, IRoom> = new Map();
  rooms.forEach((room, roomId) => {
    if (room.users.length <= size && room.status == status) {
      allRooms.set(roomId, room);
    }
  });
  return allRooms;
}

function getOne(where: { socket?: Socket; roomId?: string }) {
  const { socket, roomId } = where;
  if (socket) {
    return getRoomBySocket(socket);
  }
  if (roomId) {
    return getRoomById(roomId);
  }
  return undefined;
}

function getRoomBySocket(socket: Socket) {
  const allSocketRooms = Object.keys(socket.rooms);
  const needRoomId = allSocketRooms.find((roomId) => rooms.has(roomId));
  if (needRoomId) {
    return getRoomById(needRoomId);
  } else {
    return undefined;
  }
}

function getRoomById(roomId: string) {
  return rooms.get(roomId);
}

function deleteOne(where: { roomId?: string }) {
  const { roomId } = where;
  if (roomId) {
    const room = getOne({ roomId });
    clearTimers(room!);
    rooms.delete(roomId);
  }
}

function setDefaultRoom(where: { roomId?: string }) {
  const { roomId } = where;
  if (roomId) {
    const room = getRoomById(roomId);

    if (!room) {
      addOne({ roomId });
      return;
    }

    clearTimers(room);
    room.users.forEach((user) => {
      userModel.setDefault(user);
    });
    room.status = "created";
    room.numberOfFinishedUsers = 0;
  }
}
function clearTimers(room: IRoom) {
  const timers = room.timers;

  timers.forEach((timer) => {
    clearTimeout(timer);
  });

  room.timers = [];
}

function getResults(where: { roomId?: string; socket?: Socket }) {
  const room = getOne(where);
  if (room) {
    sortUsers(room);
    const results = room.users;
    return results;
  }
}

function sortUsers(room: IRoom) {
  room.users.sort(compareIUser);
}
function compareIUser(a: IUser, b: IUser) {
  if (a.progress == b.progress) {
    if (a.lastClick > b.lastClick) return 1;
    if (a.lastClick < b.lastClick) return -1;
    return 0;
  }
  return (a.progress - b.progress) * -1;
}

function getRoomOnline(where: { roomId?: string; room?: IRoom }) {
  const room = getOne(where);
  if (!room) return 0;
  return room.users.length;
}

function getRoomId(where: { socket: Socket }) {
  const { socket } = where;
  const roomId = Object.keys(socket.rooms).find((roomId) => rooms.has(roomId));
  return roomId;
}

function setRoomStartedStatus(where: { socket?: Socket; roomId?: string }) {
  const room = getOne(where);
  room!.status = "started";
}

function setRoomStartDate(where: { socket?: Socket; roomId?: string }) {
  const room = getOne(where);
  room!.startedTime = new Date();
}

function addTimer(where: { socket?: Socket; roomId?: string }, timer: timer) {
  const room = getOne(where);
  room!.timers.push(timer);
}

export default {
  addOne,
  getAll,
  getOne,
  deleteOne,
  setDefaultRoom,
  getRoomOnline,
  getRoomId,
  setRoomStartedStatus,
  addTimer,
  getResults,
  setRoomStartDate,
};
