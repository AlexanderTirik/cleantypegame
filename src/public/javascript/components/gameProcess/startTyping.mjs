import { NO_BREAK_SPACE } from "../../../constants/constants.mjs";

export let keyListener;

export default function startTyping(socket) {
  const selected = document.getElementById("selected-text");
  const undecorated = document.getElementById("undecorated-text");
  undecorated.innerText = sessionStorage.getItem("gameText");
  selected.innerText = undecorated.innerText[0];

  undecorated.innerText = undecorated.innerText.slice(1);

  function keyDownWrapper(event) {
    keyDown(socket, event);
  }

  document.addEventListener("keydown", keyDownWrapper);
  keyListener = keyDownWrapper;
}

function keyDown(socket, event) {
  let { colored, selected, undecorated } = getTexts();

  let pressedButton = event.key;
  if (isSpace(pressedButton)) pressedButton = NO_BREAK_SPACE;

  if (pressedButton == selected) {
    colored = colored.concat(selected);

    const click = new Date();
    requestToChangeProcess(socket, colored.length, click);

    if (undecorated.length == 30) {
      requestToFinishNear(socket);
    }

    if (undecorated.length == 0) {
      selected = "";
      setTexts({ colored, selected, undecorated });
      return;
    }

    if (undecorated[0] != " ") {
      selected = undecorated[0];
    } else {
      selected = "\xa0";
    }
    undecorated = undecorated.slice(1);
    setTexts({ colored, selected, undecorated });
  }
}

function getTexts() {
  const colored = document.getElementById("colored-text");
  const selected = document.getElementById("selected-text");
  const undecorated = document.getElementById("undecorated-text");

  return {
    colored: colored.innerText,
    selected: selected.innerText,
    undecorated: undecorated.innerText,
  };
}

function requestToChangeProcess(socket, nubmerOfLetter, lastClick) {
  const text = sessionStorage.getItem("gameText");
  const progress = calcProgress(text.length, nubmerOfLetter);
  socket.emit("SET_USER_PROGRESS", progress, lastClick);
}

function calcProgress(length, number) {
  return Math.ceil((100 * number) / length);
}

function setTexts(texts) {
  const coloredText = document.getElementById("colored-text");
  const selectedText = document.getElementById("selected-text");
  const undecoratedText = document.getElementById("undecorated-text");

  const { colored, selected, undecorated } = texts;

  coloredText.innerText = colored;
  selectedText.innerText = selected;
  undecoratedText.innerText = undecorated;
}
function isSpace(str) {
  return str == " ";
}

function requestToFinishNear(socket) {
  socket.emit("30_LETTERS_REMAINING");
}
