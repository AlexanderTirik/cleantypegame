import loginRoutes from "./loginRoutes";
import gameRoutes from "./gameRoutes";
import textRoutes from "./textRoutes"
import { Application } from "express";
export default (app: Application) => {
  app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
  app.use("/game/texts", textRoutes);
};
