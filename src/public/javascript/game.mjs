import registerSocket from "./socket/index.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

registerSocket(username);
