import { IUser } from "./userType";
import { timer } from "./timerType";

export interface IRoom {
  users: IUser[];
  timers: timer[];
  status: string;
  numberOfFinishedUsers: number;
  startedTime: Date;
}
