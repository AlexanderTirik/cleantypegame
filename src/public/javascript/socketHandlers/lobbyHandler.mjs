import lobbyPage from "../components/lobbyPage.mjs";

function joinToRoom(socket, roomId) {
  socket.emit("ADD_USER_TO_ROOM", roomId);
}

function createRoomFailed() {
  alert(
    "Oops, something went wrong. Probably name of this room already exists"
  );
}

function showExsistRooms(socket, rooms) {
  for (const [roomId, room] of Object.entries(rooms)) {
    lobbyPage.addNewRoom(socket, roomId);
  }
}

function disappearRoom(roomId) {
  lobbyPage.disappearRoom(roomId);
}

function appearRoom(socket, roomId) {
  lobbyPage.appearRoom(socket, roomId);
}

function deleteRoom(roomId) {
  lobbyPage.deleteRoom(roomId);
}

function setRoomOnline(roomId, online) {
  lobbyPage.setRoomOnlineOnPage(roomId, online);
}

export default {
  joinToRoom,
  createRoomFailed,
  showExsistRooms,
  disappearRoom,
  appearRoom,
  deleteRoom,
  setRoomOnline,
};
