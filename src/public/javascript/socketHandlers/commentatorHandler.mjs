import commentatorComponent from "../components/commentator.mjs";

function changeFace(commentatorFace) {
  commentatorComponent.setCommentator(commentatorFace);
}

function changePhrase(commentatorPhrase) {
  commentatorComponent.setPhrase(commentatorPhrase);
}

function addPhrase(commentatorPhrase) {
  commentatorComponent.addPhrase(commentatorPhrase);
}

export default { changeFace, changePhrase, addPhrase };
