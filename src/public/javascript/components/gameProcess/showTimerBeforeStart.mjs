import createElement from "../../helpers/domHelper.mjs"

export default function showTimerBeforeStart() {
  const gameBlock = document.getElementById("game-block");
  const timer = createElement({
    tagName: "div",
    className: "timer-before",
    attributes: {
      id: "timer-before",
    },
  });
  gameBlock.append(timer);
}
