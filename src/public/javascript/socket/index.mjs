import lobbyPage from "../components/lobbyPage.mjs";
import loginHandler from "../socketHandlers/loginHandler.mjs";
import lobbyHandler from "../socketHandlers/lobbyHandler.mjs";
import roomHandler from "../socketHandlers/roomHandler.mjs";
import gameHandler from "../socketHandlers/gameHandler.mjs";
import commentatorHandler from "../socketHandlers/commentatorHandler.mjs";

export default function registerSocket(username) {
  const socket = io("", { query: { username } });

  lobbyPage.initLobbyPage(socket);

  socket.on("USERNAME_INVALID", () => {
    loginHandler.auhtorizationFailed();
  });

  socket.on("CREATE_ROOM_DONE", (roomId) => {
    lobbyHandler.joinToRoom();
  });

  socket.on("CREATE_ROOM_FAIL", () => {
    lobbyHandler.createRoomFailed();
  });

  socket.on("GET_ALL_EXIST_ROOMS", (rooms) => {
    lobbyHandler.showExsistRooms(socket, rooms);
  });

  socket.on("DISAPPEAR_ROOM", (roomId) => {
    lobbyHandler.disappearRoom(roomId);
  });
  socket.on("APPEAR_ROOM", (roomId) => {
    lobbyHandler.appearRoom(socket, roomId);
  });
  socket.on("DELETE_ROOM", (roomId) => {
    lobbyHandler.deleteRoom(roomId);
  });
  socket.on("CHANGE_ROOM_ONLINE", (roomId, online) => {
    lobbyHandler.setRoomOnline(roomId, online);
  });

  socket.on("GET_ALL_USERS_IN_ROOM", (roomId, users) => {
    roomHandler.getAllUsers(socket, roomId, users);
  });
  socket.on("ADD_USER_IN_ROOM", (username, ready) => {
    roomHandler.addUser(username, ready);
  });
  socket.on("DELETE_USER_FROM_ROOM", (username) => {
    roomHandler.deleteUser();
  });
  socket.on("SWAP_READY_STATUS", (username) => {
    roomHandler.swapReadyStatus(username);
  });

  socket.on("START_GAME", () => {
    gameHandler.startGame();
  });
  socket.on("TRANSLATE_TIMER_BEFORE_START", (second) => {
    gameHandler.translateTimerBeforeStart(socket, second);
  });
  socket.on("GET_TEXT_ID", async (textId) => {
    gameHandler.requestText(textId);
  });
  socket.on("TRANSLATE_GAME_TIMER", (second) => {
    gameHandler.translateGameTimer(second);
  });
  socket.on("CHANGE_PROGRESS_BAR", (username, progress) => {
    gameHandler.changeProgress(username, progress);
  });
  socket.on("STOP_GAME", (results) => {
    gameHandler.stopGame(socket);
  });
  socket.on("CHANGE_COMMENTATOR_FACE", (commentatorFace) => {
    console.log("hello");
    commentatorHandler.changeFace(commentatorFace);
  });
  socket.on("CHANGE_COMMENTATOR_PHRASE", (commentatorPhrase) => {
    commentatorHandler.changePhrase(commentatorPhrase);
  });
  socket.on("ADD_COMMENTATOR_PHRASE", (commentatorPhrase) => {
    commentatorHandler.addPhrase(commentatorPhrase);
  });
}
