export default function changeTimerBeforeStart(second) {
  const timer = document.getElementById("timer-before");
  timer.innerText = second;
}
