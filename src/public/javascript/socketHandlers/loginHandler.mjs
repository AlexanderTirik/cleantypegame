function auhtorizationFailed() {
  alert("Something went wrong. Probably this username has already taken!");
  sessionStorage.clear();
  window.location.replace("/login");
}

export default { auhtorizationFailed };
