import createElement from "../helpers/domHelper.mjs";

function selectUserInRoom(username) {
  const userCard = document.getElementById(`${username}-user-card`);
  const newUserCardClassNames = userCard.className.concat(" you");
  userCard.className = newUserCardClassNames;
}

function addUserInRoom(username) {
  const userBlock = document.getElementById("users-block");

  let userCard = createElement({
    tagName: "div",
    className: "user-card",
    attributes: {
      id: `${username}-user-card`,
    },
  });

  userCard = appendUsernameBlock(userCard, username);
  userCard = appendProgressBar(userCard, username);

  userBlock.append(userCard);
}

function appendUsernameBlock(parentElem, username) {
  const usernameBlock = createElement({
    tagName: "div",
    className: "username-block",
  });

  const usernameElem = createElement({
    tagName: "div",
    className: "username-span",
  });
  usernameElem.innerText = username;

  const readyStatus = createElement({
    tagName: "div",
    className: "rdy-status",
    attributes: {
      id: `${username}-ready`,
    },
  });
  usernameBlock.append(usernameElem);
  usernameBlock.append(readyStatus);

  parentElem.append(usernameBlock);
  return parentElem;
}

function appendProgressBar(parentElem, username) {
  const progressBar = createElement({
    tagName: "div",
    className: "progress-bar",
  });
  const progress = createElement({
    tagName: "div",
    className: "progress",
    attributes: {
      id: `${username}-progress`,
    },
  });
  progressBar.append(progress);
  parentElem.append(progressBar);
  return parentElem;
}

function setReadyStatus(username, isReady) {
  const readyStatus = document.getElementById(`${username}-ready`);
  const oldReadyStr = isReady ? "unready" : "ready";
  const newReadyStr = isReady ? "ready" : "unready";
  if (hasClassName(readyStatus, oldReadyStr)) {
    readyStatus.className = deleteSubstring(readyStatus.className, oldReadyStr);
  }
  readyStatus.className = readyStatus.className.concat(` ${newReadyStr}`);
}

function deleteSubstring(string, substring) {
  return string.replace(substring, "");
}

function swapReadyColor(username) {
  const readyStatus = document.getElementById(`${username}-ready`);
  if (hasClassName(readyStatus, "unready")) {
    readyStatus.className = readyStatus.className.replace(" unready", " ready");
  } else {
    readyStatus.className = readyStatus.className.replace(" ready", " unready");
  }
}

function hasClassName(elem, classname) {
  return ~elem.className.indexOf(classname);
}

function deleteUserFromRoom(username) {
  const userCard = document.getElementById(`${username}-user-card`);
  userCard.remove();
}

export default {
  selectUserInRoom,
  addUserInRoom,
  setReadyStatus,
  deleteUserFromRoom,
  swapReadyColor,
};
