import loginHandler from "../socketHandlers/loginHandler";
import lobbyHandler from "../socketHandlers/lobbyHandler";
import gameHandler from "../socketHandlers/gameHandler";
import { IServer } from "../types/serverType";
import commentatorController from "../controllers/commentatorController";

export default (server: IServer) => {
  const socket = server.socket;
  socket.on("USER_AUTH", () => loginHandler.authorize(server));

  socket.on("CREATE_ROOM", (roomId: string) => {
    lobbyHandler.createRoom(server, roomId);
  });

  socket.on("GET_ONLINE_IN_ROOM", (roomId: string) =>
    lobbyHandler.getRoomOnline(server, roomId)
  );

  socket.on("ALL_LOBBY_ROOMS", () => lobbyHandler.getAllLobbyRooms(server));

  socket.on("ADD_USER_TO_ROOM", (roomId: string) =>
    gameHandler.addUserToRoom(server, roomId)
  );
  socket.on("DELETE_USER_FROM_ROOM", () =>
    gameHandler.deleteUserFromRoom(server)
  );
  socket.on("CHANGE_READY", () => gameHandler.changeUserReady(server));
  socket.on("SET_USER_PROGRESS", (progress, lastClick) =>
    gameHandler.changeUserProgress(server, progress, lastClick)
  );

  socket.on("30_LETTERS_REMAINING", () =>
    commentatorController.finishNear(server)
  );

  socket.on("disconnecting", () => {
    gameHandler.deleteUserFromRoom(server);
    loginHandler.deauthorize(server);
  });
};
