import { texts } from "../data";

function getNumberOfTexts() {
  return texts.length;
}

function getOne(where: { id: number }) {
  const { id } = where;
  return texts[id];
}

export default { getNumberOfTexts, getOne };
