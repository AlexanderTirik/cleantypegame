export const MAXIMUM_USERS_FOR_ONE_ROOM: number = 5;
export const SECONDS_TIMER_BEFORE_START_GAME: number = 3;
export const SECONDS_FOR_GAME: number = 70;
export const SECONDS_SHOW_FACT: number = 7;
export const SECONDS_SHOW_RACE_STATE: number = 30;
