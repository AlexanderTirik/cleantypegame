import createElement from "../helpers/domHelper.mjs";

function initLobbyPage(socket) {
  socket.emit("USER_AUTH");
  const createRoomButton = document.getElementById("create-room-button");

  const onClickCreateRoomButton = () => {
    let roomId = prompt("Write room name");
    if (roomId) {
      socket.emit("CREATE_ROOM", roomId);
    } else {
      alert("You must write something");
      return;
    }
  };
  createRoomButton.addEventListener("click", onClickCreateRoomButton);
  socket.emit("ALL_LOBBY_ROOMS");
}

function addNewRoom(socket, roomName) {
  const roomsBlock = document.getElementById("rooms-block");
  const roomAttribures = createRoomAttributes(socket, roomName);
  roomsBlock.append(roomAttribures);
  setOnlineRoom(socket, roomName);
}

function createRoomAttributes(socket, roomName) {
  const room = createElement({
    tagName: "div",
    className: "room-card",
    attributes: {
      id: `${roomName}-room-card`,
    },
  });

  const roomTitle = createElement({
    tagName: "span",
    className: "room-title",
  });

  roomTitle.innerText = roomName;
  const joinRoomButton = createElement({
    tagName: "button",
    className: "btn join-btn",
    attributes: {
      id: `${roomName}-join`,
    },
  });

  joinRoomButton.addEventListener("click", () =>
    socket.emit("ADD_USER_TO_ROOM", roomName)
  );

  joinRoomButton.innerText = "Join";
  const roomOnline = createElement({
    tagName: "span",
    className: "room-online",
    attributes: {
      id: `${roomName}-online`,
    },
  });

  roomOnline.innerText = "1 in room";

  room.append(roomTitle);
  room.append(roomOnline);
  room.append(joinRoomButton);

  return room;
}

function setOnlineRoom(socket, roomName) {
  socket.emit("GET_ONLINE_IN_ROOM", roomName);
}

function disappearRoom(roomId) {
  const disappearedRoom = document.getElementById(`${roomId}-room-card`);
  if (disappearRoom)
    disappearedRoom.style.display = "none";
}

function appearRoom(socket, roomId) {
  const appearedRoom = document.getElementById(`${roomId}-room-card`);

  if (appearedRoom) {
    appearedRoom.style.display = "flex";
  } else {
    addNewRoom(socket, roomId);
  }
}

function deleteRoom(roomId) {
  const destroyedRoom = document.getElementById(`${roomId}-room-card`);
  if (destroyedRoom)
    destroyedRoom.remove();
}

function setRoomOnlineOnPage(roomId, online) {
  const onlineBlock = document.getElementById(`${roomId}-online`);
  onlineBlock.innerText = `${online} in room`;
}

export default {
  initLobbyPage,
  addNewRoom,
  disappearRoom,
  appearRoom,
  deleteRoom,
  setRoomOnlineOnPage,
};
