import { IServer } from "../types/serverType";
import emitHelper from "../helpers/emitHelper";
import { roomModel } from ".";
import { commentatorStates } from "../data";

function setFace(server: IServer, face: string, roomId?: string) {
  const { socket } = server;
  if (!roomId) roomId = roomModel.getRoomId({ socket });
  emitHelper.toRoom(server, roomId!, "CHANGE_COMMENTATOR_FACE", face);
}

function setPhrase(server: IServer, phase: string, roomId?: string) {
  const { socket } = server;
  if (!roomId) roomId = roomModel.getRoomId({ socket });
  emitHelper.toRoom(server, roomId!, "CHANGE_COMMENTATOR_PHRASE", phase);
}

function addPhrase(server: IServer, phrase: string, roomId?: string) {
  const { socket } = server;
  if (!roomId) roomId = roomModel.getRoomId({ socket });
  emitHelper.toRoom(server, roomId!, "ADD_COMMENTATOR_PHRASE", phrase);
}

function getState(state: string) {
  return commentatorStates.get(state);
}
function getRandomFact() {
  const facts = commentatorStates.get("facts")!.phrase;
  return facts[Math.floor(Math.random() * facts.length)];
}

export default { setFace, setPhrase, addPhrase, getState, getRandomFact };
