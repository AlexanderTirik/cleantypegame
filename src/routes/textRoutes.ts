import { Router, Request, Response } from "express";
import { textModel } from "../models";

const router = Router();

router.get("/:id", (req: Request, res: Response) => {
  const id: number = +req.params.id;
  const text = textModel.getOne({ id });
  
  const textResponse: { error: boolean; data: string } = {
    error: false,
    data: text,
  };
  res.status(200).send(textResponse);
});

export default router;
