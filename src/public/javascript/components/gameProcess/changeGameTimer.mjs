export default function changeGameTimer(second) {
  const timer = document.getElementById("game-timer");
  timer.innerText = `${second} seconds left`;
}
