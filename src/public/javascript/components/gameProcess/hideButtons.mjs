export default function hideButtons() {
  const readyButton = document.getElementById("ready-button");
  const backButton = document.getElementById("back-button");
  readyButton.style.display = "none";
  backButton.style.display = "none";
}
