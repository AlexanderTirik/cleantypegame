import { Socket } from "socket.io";
import { onlineModel, userModel } from "../models";
import { IServer } from "../types/serverType";
import emitHelper from "../helpers/emitHelper";

function authorize(server: IServer) {
  const { socket } = server;
  
  if (!isOnline(socket)) {
    onlineModel.addOne({ socket });
  } else {
    emitHelper.toSender(server, "USERNAME_INVALID");
  }
}

function isOnline(socket: Socket): boolean {
  const userOnline = onlineModel.getOne({ socket });
  if (userOnline) return true;
  else return false;
}

function deauthorize(server: IServer) {
  const { socket } = server;
  if (verifyOnline(socket)) {
    onlineModel.deleteOne({ socket });
  }
}

function verifyOnline(socket: Socket) {
  const userId = socket.id;
  const userOnline = onlineModel.getOne({ socket });

  if (userOnline == userId) {
    return true;
  } else {
    return false;
  }
}

export default { authorize, deauthorize };
