import { roomModel } from "../models";
import { IServer } from "../types/serverType";
import emitHelper from "../helpers/emitHelper";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";
import gameHandler from "./gameHandler";

function createRoom(server: IServer, roomId: string) {
  const { socket } = server;
  const existRoom = roomModel.getOne({ roomId });
  if (existRoom) {
    emitHelper.toSender(server, "CREATE_ROOM_FAIL");
  } else {
    emitHelper.toServer(server, "APPEAR_ROOM", roomId);
    roomModel.addOne({ roomId });

    gameHandler.addUserToRoom(server, roomId);
  }
}

function getRoomOnline(server: IServer, roomId: string) {
  const roomOnline = roomModel.getRoomOnline({ roomId });
  emitHelper.toSender(server, "CHANGE_ROOM_ONLINE", roomId, roomOnline);
}

function getAllLobbyRooms(server: IServer) {
  const allRooms = roomModel.getAll({
    status: "created",
    size: MAXIMUM_USERS_FOR_ONE_ROOM - 1,
  });
  const allRoomsObj = Object.fromEntries(allRooms);

  emitHelper.toSender(server, "GET_ALL_EXIST_ROOMS", allRoomsObj);
}

export default { createRoom, getRoomOnline, getAllLobbyRooms };
