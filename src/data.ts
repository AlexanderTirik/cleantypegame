import { IRoom } from "./types/roomType";
import { ICommentator } from "./types/commentatorType";

export const texts: string[] = [
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
];

export default { texts };

export const rooms: Map<string, IRoom> = new Map();

export const online: Map<string, string> = new Map();

export const commentatorStates: Map<string, ICommentator> = new Map([
  [
    "greeting",
    {
      face: " (^•^)/ ",
      phrase: [
        "Ghbdtn.... Oh... Hello everyone! I’m Key Board, welcome  to the world’s greatest  TypeRacing Championship!!! ",
      ],
    },
  ],
  [
    "presentation",
    {
      face: " ヽ(^o^) ",
      phrase: [
        "Let’s greet competitors, they are the best of the best, so meet them with applause. \n They are: \n",
      ],
    },
  ],
  [
    "startTimer",
    {
      face: " (*^_^*)",
      phrase: [
        "While our racers start the race, pushing their CapsLocks, Enters, Ctrl and Shifts l can’t wait to see this exciting struggle for the title of the best TypeRacer in the World or if you like of neat-handed and speedy genius.",
      ],
    },
  ],
  [
    "state1",
    {
      face: "(＾▽＾) ",
      phrase: [
        "We’ve got an excellent state of race. First place: ",
        ", he is hard chased by ",
        " and ",
        ".",
      ],
    },
  ],
  [
    "state2",
    {
      face: "ヽ(・∀・)ﾉ",
      phrase: [
        "Watch ",
        " he is in first place. ",
        " goes behind him. ",
        " in third place with a decent gap. ",
      ],
    },
  ],
  [
    "finishNear",
    {
      face: "(*^о^*)/",
      phrase: [
        "Let’s look at the gap now, ",
        " is leading the race. ",
        " has got closer, but has the second position. ",
        " focuses on his Space Bar pedal.... At this point there are 30 symbols til the end of the race ",
      ],
    },
  ],
  ["onFinish", { face: "(ﾉ◕ヮ◕)ﾉ", phrase: ["Hey!! ", " finished!!!"] }],
  [
    "results",
    {
      face: "＼(￣▽￣)／",
      phrase: [
        "Here is the moment for the final step and YEAH ",
        " is first to cross the finish line, ",
        " has the second place with the ",
        " seconds gap and ",
        "  comes last with ",
        " seconds gap.  ",
      ],
    },
  ],
  [
    "facts",
    {
      face: "(´･ᴗ･ )",
      phrase: [
        "Did you know that every second, spacebars on keyboards around the world are hit about six million times.",
        "By the way, When you have a great speed of typing, it helps you in reducing your stress.",
        "The  father of modern day keyboard is the model M, produced by IBM and Lexmark in 1984",
        "Someone made a monument for QWERTY keyboard, it is located in the city of Yekaterinburg, Russia",
      ],
    },
  ],
]);

export const transports: string[] = ["Porsche", "Lamborghini", "MacLaren"];
