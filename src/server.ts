import express, { Request, Response } from "express";
import http from "http";
import socketIO from "socket.io";
import routes from "./routes/index";
import { STATIC_PATH, PORT } from "./config";
import socketSetup from "./socket"

const app = express();
const httpServer = new http.Server(app);

app.use(express.static(STATIC_PATH));
routes(app);

app.get("*", (req: Request, res: Response) => {
  res.redirect("/login");
});

socketSetup(httpServer)

app.use(express.json());

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
