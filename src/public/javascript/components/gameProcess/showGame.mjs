import createElement from "../../helpers/domHelper.mjs"
export default function showGame(text) {
  createGameTimer();
  createTextArea(text);
}

function createGameTimer() {
  const gameBlock = document.getElementById("game-block");
  const gameTimer = createElement({
    tagName: "span",
    className: "game-timer",
    attributes: {
      id: "game-timer",
    },
  });
  gameBlock.append(gameTimer);
}

function createTextArea(text) {
  const gameBlock = document.getElementById("game-block");
  const textBlock = createElement({
    tagName: "div",
    className: "",
    attributes: {
      id: "text-block",
    },
  });
  const coloredText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "colored-text",
    },
  });
  const selectedText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "selected-text",
    },
  });
  const undecoratedText = createElement({
    tagName: "span",
    className: "",
    attributes: {
      id: "undecorated-text",
    },
  });

  undecoratedText.innerText = text;

  textBlock.append(coloredText);
  textBlock.append(selectedText);
  textBlock.append(undecoratedText);
  gameBlock.append(textBlock);
}
