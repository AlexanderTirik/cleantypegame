export interface IUser {
  id: string;
  username: string;
  progress: number;
  ready: boolean;
  lastClick: Date;
}
