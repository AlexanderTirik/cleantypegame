import createElement from "../helpers/domHelper.mjs";
import userInRoom from "./userInRoom.mjs";

function initGamePage(socket, username, roomName) {
  createRoomInfoElement(socket, roomName);
  hideRoomPage();
  showGamePage();
  createReadyButton(socket, username);
  userInRoom.selectUserInRoom(username);
}

function createRoomInfoElement(socket, roomName) {
  const roomInfo = document.getElementById("room-info");
  const backButton = createBackButton(socket);
  const roomNameElem = createElement({
    tagName: "div",
    className: "room-name",
  });
  roomNameElem.innerText = `Room: ${roomName}`;

  roomInfo.append(backButton);
  roomInfo.append(roomNameElem);
}

function createBackButton(socket) {
  const backButton = createElement({
    tagName: "button",
    className: "back-button btn",
    attributes: {
      id: "back-button",
    },
  });
  backButton.innerText = "Back To Rooms";
  backButton.addEventListener("click", () => backToLobbyClick(socket));
  return backButton;
}

function backToLobbyClick(socket) {
  clearGamePage();
  showRoomPage();
  socket.emit("DELETE_USER_FROM_ROOM");
}

function clearGamePage() {
  const gamePage = document.getElementById("game-page");
  const roomInfo = document.getElementById("room-info");
  const usersBlock = document.getElementById("users-block");
  const gameBlock = document.getElementById("game-block");
  roomInfo.innerHTML = "";
  gameBlock.innerHTML = "";
  usersBlock.innerHTML = "";
  gamePage.style.display = "none";
}

function showRoomPage() {
  const roomPage = document.getElementById("rooms-page");
  roomPage.style.display = "block";
}

function hideRoomPage() {
  const roomPage = document.getElementById("rooms-page");
  roomPage.style.display = "none";
}

function showGamePage() {
  const gamePage = document.getElementById("game-page");

  gamePage.style.display = "flex";
}

function createReadyButton(socket, username) {
  const gameBlock = document.getElementById("game-block");

  const readyButton = createElement({
    tagName: "button",
    className: "ready-button btn",
    attributes: {
      id: `ready-button`,
    },
  });
  readyButton.innerText = "READY";
  readyButton.addEventListener("click", () => changeReadyClick(socket));

  gameBlock.append(readyButton);
}

function changeReadyClick(socket) {
  socket.emit("CHANGE_READY");
  const readyButton = document.getElementById("ready-button");
  if (readyButton.innerText == "READY") {
    readyButton.innerText = "NOT READY";
  } else {
    readyButton.innerText = "READY";
  }
}

export default initGamePage;
