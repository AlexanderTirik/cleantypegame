export interface ICommentator {
  face: string;
  phrase: string[];
}
