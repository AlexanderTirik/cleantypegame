import { roomModel } from "./index";
import { Socket } from "socket.io";
import { IUser } from "../types/userType";
import { IRoom } from "../types/roomType";

function createUser(where: { socket: Socket }) {
  const { socket } = where;
  const username = socket.handshake.query.username;
  const user: IUser = {
    id: socket.id,
    username,
    progress: 0,
    ready: false,
    lastClick: new Date(0),
  };
  return user;
}

function joinUserToRoom(room: IRoom, user: IUser) {
  room.users.push(user);
}

function addToRoom(where: { room?: IRoom; roomId?: string; socket: Socket }) {
  let { room, roomId, socket } = where;
  const user = createUser({ socket });
  if (!room) {
    room = roomModel.getOne({ roomId });
  }
  joinUserToRoom(room!, user);
}

function getOne(where: { socket?: Socket }) {
  const { socket } = where;
  if (socket) {
    return getBySocket(socket);
  }

  return undefined;
}

function getBySocket(socket: Socket) {
  const userRoom = roomModel.getOne({ socket });
  if (userRoom) {
    const user = userRoom.users.find((user) => {
      if (user.id == socket.id) return true;
    });
    return user;
  }
  return undefined;
}

function setDefault(user: IUser): void {
  if (user) {
    user.progress = 0;
    user.ready = false;
    user.lastClick = new Date(0);
  }
}

function deleteFromRoom(where: { socket: Socket }) {
  const { socket } = where;
  const room = roomModel.getOne({ socket });
  if (room) {
    const user = getOne({ socket });
    const userIndex = room.users.indexOf(user!);
    if (userIndex != -1) {
      room.users.splice(userIndex, 1);
    }
  }
}

function getUsername(where: { socket: Socket }) {
  const { socket } = where;
  return socket.handshake.query.username;
}

function getAllGameLobbyUsers(where: {
  roomId?: string;
  room?: IRoom;
  socket?: Socket;
}) {
  const room = roomModel.getOne(where);
  let usernames: { username: string; ready: boolean }[] = [];
  if (room) {
    room.users.forEach((user) => {
      usernames.push({
        username: user.username,
        ready: user.ready,
      });
    });
  }
  return usernames;
}
export default {
  getOne,
  setDefault,
  addToRoom,
  deleteFromRoom,
  getUsername,
  getAllGameLobbyUsers,
};
