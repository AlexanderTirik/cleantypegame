import { Socket } from "socket.io";
import { online } from "../data";
import { userModel } from ".";

function addOne(where: { socket: Socket }) {
  const { socket } = where;
  const username = socket.handshake.query.username;
  const userId = socket.id;
  online.set(username, userId);
}

function getOne(where: { socket: Socket }) {
  const { socket } = where; 
  const username = userModel.getUsername({ socket });
  return online.get(username);
}

function deleteOne(where: { socket: Socket }) {
  const { socket } = where;
  const username = userModel.getUsername({ socket });
  online.delete(username);
}

function has(where: { socket: Socket }) {
  const { socket } = where;
  const username = userModel.getUsername({ socket });
  return online.has(username);
}

export default { addOne, getOne, deleteOne, has };
