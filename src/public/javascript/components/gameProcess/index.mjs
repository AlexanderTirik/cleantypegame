import hideButtons from "./hideButtons.mjs"; // etot
import showTimerBeforeStart from "./showTimerBeforeStart.mjs"; // etot
import changeTimerBeforeStart from "./changeTimerBeforeStart.mjs";
import hideTimerBeforeStart from "./hideTimerBeforeStart.mjs";
import isTimerFinished from "./isTimerFinished.mjs";
import showGame from "./showGame.mjs";
import changeGameTimer from "./changeGameTimer.mjs";
import stopGame from "./stopGame.mjs";
import startTyping from "./startTyping.mjs";
import changeProgressBar from "./changeUserProgress.mjs";

export default {
  hideButtons,
  showTimerBeforeStart,
  changeTimerBeforeStart,
  hideTimerBeforeStart,
  isTimerFinished,
  showGame,
  changeGameTimer,
  stopGame,
  startTyping,
  changeProgressBar,
};
