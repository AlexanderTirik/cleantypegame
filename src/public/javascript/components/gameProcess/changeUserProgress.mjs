export default function changeProgressBar(username, progress) {
  const progressBar = document.getElementById(`${username}-progress`);
  progressBar.style.width = `${progress}%`;
  changeProgressBarColor(progressBar, progress);
}

function changeProgressBarColor(progressBar, progress) {
  if (progress == 100) {
    progressBar.style.backgroundColor = "greenyellow";
  }
  if (progress < 100) {
    progressBar.style.backgroundColor = "green";
  }
}
