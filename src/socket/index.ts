import socketIO from "socket.io";
import registerSockets from "./game";
import { IServer } from "../types/serverType";
import { Server as httpServer } from "http";

export default function createSocket(app: httpServer) {
  const io = socketIO(app);
  io.on("connection", (socket) => {
    const server: IServer = {
      io,
      socket,
    };
    registerSockets(server);
  });
}
